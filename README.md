# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.
### Author ###
 Sydney Hollingsworth, sholling@uoregon.edu

### Program Function ###

A tiny web server in Python, to denomstrate understanding of basic web architecture. A socket is created to listen for requests. When a request is recieved it checks for a properly formatted URL, and either sends the contents to the client, or responds with the correct error.

* If the URL ends with `name.html` or `name.css` (i.e., if `path/to/name.html` is in document path (from DOCROOT)), it responds with proper http response. 
* If `name.html` is not in current directory, it responds with 404 (not found). 
* If a page contains one of the symbols(~ // ..), it responds with 403 forbidden error. For example, `url=localhost:5000/..name.html` or `/~name.html` would give 403 forbidden error.

* If no file is given, the client is served a cat picture. 
```
  ^ ^
=(   )=
```


### What do I need?  Where will it work? ###

* Designed for Unix, mostly interoperable on Linux (Ubuntu) or MacOS. May also work on Windows, but no promises. 

* You will need Python version 3.4 or higher. 

* Designed to work in "user mode" (unprivileged), therefore using a port number above 1000 (rather than port 80 that a privileged web server would use)

  
## Usage ##
To start the server use commands
  ```
  make run
  ```
  or
  ```
  make start
  ```
To stop the server use command

  ```
  make stop
  ```
  
Alternatively, automated tests can be performed using the script tests.sh using

```
tests.sh http://the.IP.address:port/path
```



